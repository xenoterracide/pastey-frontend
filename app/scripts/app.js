define([
	'angular',
	'angular-route',
	'angular-sanitize',
	'routes',
	'restangular',
], function( angular ) {
	'use strict';

	return angular.module('pasteyApp',[
		'ngRoute',
		'ngSanitize',
		'restangular',
	]);
});
