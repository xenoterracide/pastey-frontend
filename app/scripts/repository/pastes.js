define(['restangular'], function ( ) {
	'use strict';
	return [  '$q', '$log', 'Restangular',
	function ( $q,   $log,   Restangular ) {
		var rs = Restangular.withConfig( function( Configurer ) {
				Configurer.setBaseUrl('http://localhost:5984');
			});

		return {
			_cache : {},
			size : function () {
			},
			get  : function ( key ) {
				var paste = $q.defer();
				$log.debug( 'get: ', this._cache );
				if ( typeof this._cache[key] == 'object' ) {
					paste.resolve( this._cache[key] );
				}
				else {
					paste.resolve( rs.one( 'pastes', key ).get() );
				}
				return paste.promise;
			},
			set : function ( key, value ) {
				var paste = { code : value };

				$log.debug( 'set: ', key, paste);

				rs.one( 'pastes', key ).customPUT( paste );

				this._cache[key] = paste;

				$log.debug( 'set: ' , this._cache );
			},
			delete : function ( key ) {
			},
			has : function ( key ) {
				var def = $q.defer();
				if ( typeof this._cache[key] == 'object' ) {
					def.resolve(true);
				}
				else {
					rs.one( 'pastes', key ).head().then(
						function () {
							$log.debug( 'head good' );
							def.resolve(true);
						},
						function() {
							$log.debug( 'head error' );
							def.reject(false);
						}
					);
				}
				return def.promise;
			},
			clear : function () {
			},
		};
	}];
});
