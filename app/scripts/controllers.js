define([
	'app',
	'services',
], function ( app ) {
	'use strict';

	return app
		.controller('Insert', ['$scope','$injector', function( $scope, $injector ) {
			require(['controllers/insert'], function( insert ) {
				$injector.invoke( insert, this, { '$scope': $scope });
			});
		}])
		.controller('Render', ['$scope','$injector', function( $scope, $injector ) {
			require(['controllers/render'], function( render ) {
				$injector.invoke( render, this, { '$scope': $scope });
			});
		}])
		;
});
