define([
	'app',
	'repository/pastes',
], function( app, pastes ) {
	'use strict';

	return app.service('pastes', pastes );
});
