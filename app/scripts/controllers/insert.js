define([
	'crypto.MD5',
	'controllers',
	'services',
], function() {
	'use strict';

	return ['$scope', '$location', 'pastes', function ( $scope, $location, pastes ) {
		$scope.view = function( ) {
			if ( $scope.code ) {
				// boo hex, really want urlsafe base64
				var digest = Crypto.MD5( $scope.code ).toString();
				pastes.set( digest, $scope.code );
				$location.path( digest );
			}
		};
	}];
});
