define([
	'highlight',
	'angular',
], function( hl ) {
	'use strict';

	return [      '$log', '$scope', '$location', '$routeParams', 'pastes',
		function ( $log,   $scope,   $location,   $routeParams,   pastes ) {
			var digest  = $routeParams.digest;
			$log.debug( 'render: ', digest );

			pastes.has( digest ).then(function ( ) {
				pastes.get( digest ).then( function ( paste ) {;
					$log.debug( 'code: ', paste.code );
					$scope.code = hl.highlightAuto( paste.code ).value;
					$log.debug( $scope.code );
				});
			});

			$scope.view = function( view ) { $location.path( view ); };
			$scope.$apply();
		}];
});
