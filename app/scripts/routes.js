define([
	'app',
	'controllers',
], function ( app ) {
	'use strict';

	return app.config(function ($routeProvider) {
		$routeProvider
		.when('/', {
			templateUrl: 'views/insert.html',
			controller: 'Insert'
		})
		.when('/about', {
			templateUrl: 'views/about.html',
		})
		.when('/contact', {
			templateUrl: 'views/contact.html',
		})
		.when('/:digest', { // order here matters
			templateUrl: 'views/render.html',
			controller: 'Render'
		})
		.otherwise({
			redirectTo: '/'
		});
	});
});
