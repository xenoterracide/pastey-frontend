require.config({
	baseUrl  :'/scripts',
	deps     :['pastey','bootstrap'],
	paths: {
		'domReady'        :'../bower_components/requirejs-domready/domReady',
		'jquery'          :'../bower_components/jquery/jquery',
		'angular'         :'../bower_components/angular/angular',
		'angular-sanitize':'../bower_components/angular-sanitize/angular-sanitize',
		'angular-route'   :'../bower_components/angular-route/angular-route',
		'bootstrap'       :'../bower_components/bootstrap/dist/js/bootstrap',
		'crypto'          :'../bower_components/cryptojs/lib/Crypto',
		'crypto.MD5'      :'../bower_components/cryptojs/lib/MD5',
		'highlight'       :'../bower_components/highlightjs/highlight.pack',
		'restangular'     :'../bower_components/restangular/dist/restangular',
		'underscore'      :'../bower_components/lodash/dist/lodash.underscore',
	},
	shim: {
		'angular'         : { 'exports':'angular',     'deps':['jquery']  },
		'highlight'       : { 'exports':'hljs'                            },
		'restangular'     : {
			'exports':'Restangular',
			'deps'   :['angular','underscore'],
		},
		'angular-sanitize': [ 'angular' ],
		'angular-route'   : [ 'angular' ],
		'bootstrap'       : [ 'jquery'  ],
		'crypto.MD5'      : [ 'crypto'  ],
	},
});
