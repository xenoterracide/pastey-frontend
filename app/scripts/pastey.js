define([
	'require',
	'angular',
	'routes', // requires controllers and app
],
function ( require, angular ) {
	'use strict';


	require(['domReady!'], function(document) {
		angular.bootstrap(document, ['pasteyApp']);
	});
});
