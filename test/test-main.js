var tests = [];
for (var file in window.__karma__.files) {
	if (/spec\.js$/i.test(file)) {
		tests.push(file);
	}
}

require.config({
    baseUrl: '/base/app/scripts',
	paths: {
		'domReady'        :'../bower_components/requirejs-domready/domReady',
		'jquery'          :'../bower_components/jquery/jquery',
		'angular'         :'../bower_components/angular/angular',
		'angular-sanitize':'../bower_components/angular-sanitize/angular-sanitize',
		'angular-route'   :'../bower_components/angular-route/angular-route',
		'angular-mocks'   :'../bower_components/angular-mocks/angular-mocks',
		'crypto'          :'../bower_components/cryptojs/lib/Crypto',
		'crypto.MD5'      :'../bower_components/cryptojs/lib/MD5',
		'highlight'       :'../bower_components/highlightjs/highlight.pack',
		'restangular'     :'../bower_components/restangular/dist/restangular',
		'underscore'      :'../bower_components/lodash/dist/lodash.underscore',
	},
	shim: {
		'angular'         : { 'exports':'angular',     'deps':['jquery']  },
		'underscore'      : { 'exports':'_',                              },
		'highlight'       : { 'exports':'hljs'                            },
		'restangular'     : {
			'exports':'Restangular',
			'deps'   :['angular','underscore'],
		},
		'angular-sanitize': [ 'angular' ],
		'angular-route'   : [ 'angular' ],
		'angular-mocks'   : [ 'angular' ],
		'crypto.MD5'      : [ 'crypto'  ],
	},
    deps: ['pastey','/base/test/spec/controllers/insert.js'],
	callback: window.__karma__.start,
});
