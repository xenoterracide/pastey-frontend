define([
	'angular-mocks',
	'controllers/insert'],
function( mocks, app, controller ) {
	'use strict';

	describe('Controller: Insert', function () {
		// load the controller's module
		beforeEach(module('pasteyApp'));

		var scope,location,controller,pastes;
		var code = 'package Foo::Bar;';
		var md5  = '3017ba6b67fffa945b4a6de24b9950bc';

		// Initialize the controller and a mock scope
		beforeEach(inject(function ( $injector, $rootScope ) {
			pastes     = $injector.get('pastes');
			location   = $injector.get('$location');
			controller = $injector.get('$controller');
			scope      = $rootScope.$new();
			scope.code = code;

			var Insert = controller('Insert', {
				$scope:    scope,
				$location: location,
				pastes:    pastes,
			});

			waits(500);
		}));

		it('has code', function () {
				expect( scope.code ).toBe('package Foo::Bar;');
				scope.view();
				expect( location.path() ).not.toBe('');
				expect( location.path() ).toBe('/' + md5 );
				expect( pastes[md5] ).toBe( code );
		});
	});

});
